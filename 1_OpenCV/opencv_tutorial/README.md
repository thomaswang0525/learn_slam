# OpenCV入门教程

[TOC]

## 1. OpenCV介绍
OpenCV 的全称是 Open Source Computer Vision Library,是一个开放源代码的 计算机视觉库。OpenCV 是最初由英特尔公司发起并开发,以 BSD 许可证授权发行,可以在商业和研究领域中免费使用,现在美国 Willow Garage 为OpenCV 提供主要的支持。OpenCV 可用于开发实时的图像处理、计算机视觉以及模式识别程序,目前在工业界以及科研领域广泛采用。 简单了说,OpenCV 其实就是一堆 C 和 C++语言的源代码文件,这些源代码文件中实现了许多常用的计算机视觉算法。例如 C 接口函数 cvCanny()实现了 Canny 边缘提取算法。可以直接将这些源代码添加到我们自己的软件项目中,而不需要自己再去写代码实现 Canny 算法,也就是不需要重复“造轮子”。

## 2 . OpenCV安装
> 注:OpenCV 是一个功能强大的计算机视觉库,要用好它,除了要具有相关的计算机视觉理论知识外,还需要具有一定的编程能力。 前置课程:[cpp/cpp11](../../cpp/cpp11/README.md)

安装OpenCV的依赖库：

```
sudo apt-get install build-essential

sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev 
libswscale-dev3 

sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev 
```

gitee上搜索并下载OpenCV源码：

```
cd ~/opencv
mkdir build
cd build
cmake .. 
make
sudo make install
```

可能遇到的问题：编译过程中ippicv下载失败， 可以下载该文件（保存在文件夹open/3rdparty/ippicv内） ，并将其放在对应的文件夹内，并该文件夹内的ippicv.cmake文件修改

```
"https://raw.githubusercontent.com/opencv/opencv_3rdparty/${IPPICV_COMMIT}ippicv/"
# 改为 "../3rdparty/ippicv/ippicv.cmake  #（仅供参考，根据自己的路径填写）
```

Python-OpenCV安装较为简单，仅需在终端输入：

```
pip3 install opencv-python
```

即可，使用时仅需要输入

```
import cv2
```



## 3 . 图像的基本操作

### 3.1 图像的表示
在正式介绍之前,先简单介绍一下数字图像的基本概念。如图 3.1 中所示的图像,我们看到的是 Lena 的头像,但是计算机看来,这副图像只是一堆亮度各异的点。一副尺寸为 M × N 的图像可以用一个 M × N 的矩阵来表示,矩阵元素的值表示这个位置上的像素的亮度,一般来说像素值越大表示该点越亮。

<img src='./images/Fig1.png' width='300' height='300'>

**图 3.1 Lena's photo**

如图 3.1 中白色圆圈内的区域,进行放大并仔细查看,将会如图 3.2 所示。

<img src='./images/Fig2.png' width='150' height='150'>

**图 3.2 图 3.1 中圆圈处的放大效果**

一般来说,灰度图用 2 维矩阵表示,彩色(多通道)图像用 3 维矩阵(M× N × 3)表示。对于图像显示来说,目前大部分设备都是用无符号 8 位整数(类型为 CV_8U)表示像素亮度。 图像数据在计算机内存中的存储顺序为以图像最左上点(也可能是最左下点)开始,存储如表 3-1 所示。

<img src='./images/table1.png' width='250' height='250'>

I<sub> i j</sub> 表示第 i 行 j 列的像素值。如果是多通道图像,比如 RGB 图像,则每个像素用三个字节表示。在 OpenCV 中,RGB 图像的通道顺序为 BGR ,存储如表 3-2 所示。

<img src='./images/table2.png' width='320' height='170'>


### 3.2. Mat类
早期的 OpenCV 中,使用 IplImage 和 CvMat 数据结构来表示图像。IplImage和 CvMat 都是 C 语言的结构。使用这两个结构的问题是内存需要手动管理,开发者必须清楚的知道何时需要申请内存,何时需要释放内存。这个开发者带来了一定的负担,开发者应该将更多精力用于算法设计,因此在新版本的 OpenCV 中引入了 Mat 类。 新加入的 Mat 类能够自动管理内存。使用 Mat 类,你不再需要花费大量精力在内存管理上。而且你的代码会变得很简洁,代码行数会变少。但 C++接口唯一的不足是当前一些嵌入式开发系统可能只支持 C 语言,如果你的开发平台支持C++,完全没有必要再用 IplImage 和 CvMat。在新版本的 OpenCV 中,开发者依然可以使用 IplImage 和 CvMat,但是一些新增加的函数只提供了 Mat 接口。这里的例程也都将采用新的 Mat 类,不再介绍 IplImage 和 CvMat。Mat 类的定义如下所示,关键的属性如下方代码所示:

```
class CV_EXPORTS Mat
{
public:
    //一系列函数
    ...
    /* flag 参数中包含许多关于矩阵的信息,如:
-Mat 的标识
-数据是否连续
-深度
-通道数目
*/
    int flags;
    //矩阵的维数,取值应该大于或等于 2
    int dims;
    //矩阵的行数和列数,如果矩阵超过 2 维,这两个变量的值都为-1
    int rows, cols;
    //指向数据的指针
    uchar* data;
    //指向引用计数的指针
    //如果数据是由用户分配的,则为 NULL
    int* refcount;
    //其他成员变量和成员函数
    ...
};
```

### 3.3创建 Mat 对象
Mat 是一个非常优秀的图像类,它同时也是一个通用的矩阵类,可以用来创建和操作多维矩阵。有多种方法创建一个Mat 对象。

#### 3.3.1 构造函数方法
Mat 类提供了一系列构造函数,可以方便的根据需要创建 Mat 对象。下面是一个使用构造函数创建对象的例子。
```
Mat M(3,2, CV_8UC3, Scalar(0,0,255));
cout << "M = " << endl << " " << M << endl;
```
第一行代码创建一个行数(高度)为 3,列数(宽度)为 2 的图像,图像元素是 8 位无符号整数类型,且有三个通道。图像的所有像素值被初始化为(0, 0,255)。由于 OpenCV 中默认的颜色顺序为 BGR,因此这是一个全红色的图像。 第二行代码是输出 Mat 类的实例 M 的所有像素值。 Mat 重定义了<<操作符,使用这个操作符,可以方便地输出所有像素值,而不需要使用 for 循环逐个像素输出。 该段代码的输出如图 3.3 所示。

<img src='./images/Fig3.png' width='422' height='164'>

**图 3.3 例程输出内容 **

常用的构造函数有:
- Mat::Mat()--------无参数构造方法;
- Mat::Mat(int rows, int cols, int type)--------创建行数为 rows,列数为 col,类型为 type 的图像;
- Mat::Mat(Size size, int type)--------创建大小为 size,类型为 type 的图像;
- Mat::Mat(int rows, int cols, int type, const Scalar& s)--------创建行数为 rows,列数为 col,类型为 type 的图像,并将所有元素初始化为值 s;
- Mat::Mat(Size size, int type, const Scalar& s)--------创建大小为 size,类型为 type 的图像,并将所有元素初始化为值 s;
- Mat::Mat(const Mat& m)--------将 m 赋值给新创建的对象,此处不会对图像数据进行复制, m 和新对象共用图像数据;
- Mat::Mat(int rows, int cols, int type, void* data, size_t step=AUTO_STEP)--------创建行数为 rows,列数为col,类型为 type 的图像,此构造函数不创建图像数据所需内存,而是直接使用 data 所指内存,图像的行步长由step指定。
- Mat::Mat(Size size, int type, void* data, size_t step=AUTO_STEP)--------创建大小为 size,类型为 type 的图像,此构造函数不创建图像数据所需内存,而是直接使用 data 所指内存,图像的行步长由 step 指定。
- Mat::Mat(const Mat& m, const Range& rowRange, const Range& colRange)--------创建的新图像为 m 的一部分,具体的范围由 rowRange 和 colRange 指定,此构造函数也不进行图像数据的复制操作,新图像与 m共用图像数据;
- Mat::Mat(const Mat& m, const Rect& roi)--------创建的新图像为 m 的一部分,具体的范围 roi 指定,此构造函数也不进行图像数据的复制操作,新图像与 m 共用图像数据。

这些构造函数中,很多都涉及到类型 type。 type 可以是 CV_8UC1, CV_16SC1, ...,CV_64FC4 等。里面的 8U 表示 8 位无符号整数,16S 表示 16 位有符号整数,64F表示 64 位浮点数(即 double 类型);C 后面的数表示通道数,例如 C1 表示一个通道的图像,C4 表示 4 个通道的图像,以此类推。如果你需要更多的通道数,需要用宏 CV_8UC(n),例如:
```
Mat M(3,2, CV_8UC(5));//创建行数为 3,列数为 2,通道数为 5 的图像
```

#### 3.3.2 create()函数创建对象
除了在构造函数中可以创建图像,也可以使用 Mat 类的 create()函数创建图像。如果 create()函数指定的参数与图像之前的参数相同,则不进行实质的内存申请操作;如果参数不同,则减少原始数据内存的索引,并重新申请内存。使用方法如下面例程所示:
```
Mat M(2,2, CV_8UC3);//构造函数创建图像
M.create(3,2, CV_8UC2);//释放内存重新创建图像
```

需要注意的时,使用 create()函数无法设置图像像素的初始值。

#### 3.3.3 Matlab 风格的创建对象方法
OpenCV 2 中提供了 Matlab 风格的函数,如 zeros(),ones()和 eyes()。这种方法使得代码非常简洁,使用起来也非常方便。使用这些函数需要指定图像的大小和类型,使用方法如下:
```
Mat Z = Mat::zeros(2,3, CV_8UC1);
cout << "Z = " << endl << " " << Z << endl;

Mat O = Mat::ones(2, 3, CV_32F);
cout << "O = " << endl << " " << O << endl;

Mat E = Mat::eye(2, 3, CV_64F);
cout << "E = " << endl << " " << E << endl;
```
该代码中,有些 type 参数如 CV_32F 未注明通道数目,这种情况下它表示单通道。上面代码的输出结果如图 3.4 所示。

<img src='./images/Fig5.png' width='204' height='370'>

**图 3.4 Matlab 风格的函数例程的输出结果**

### 3.4矩阵的基本元素表达
对于单通道图像,其元素类型一般为 8U(即 8 位无符号整数),当然也可以是 16S、32F 等;这些类型可以直接用uchar、short、float 等 C/C++语言中的基本 数据类型表达。 如果多通道图像,如 RGB 彩色图像,需要用三个通道来表示。在这种情况下,如果依然将图像视作一个二维矩阵,那么矩阵的元素不再是基本的数据类型。 OpenCV 中有模板类Vec,可以表示一个向量。OpenCV 中使用 Vec 类预定义了一些小向量,可以将之用于矩阵元素的表达。
```
typedef Vec<uchar, 2> Vec2b;
typedef Vec<uchar, 3> Vec3b;
typedef Vec<uchar, 4> Vec4b;

typedef Vec<short, 2> Vec2s;
typedef Vec<short, 3> Vec3s;
typedef Vec<short, 4> Vec4s;

typedef Vec<int, 2> Vec2i;
typedef Vec<int, 3> Vec3i;
typedef Vec<int, 4> Vec4i;

typedef Vec<float, 2> Vec2f;
typedef Vec<float, 3> Vec3f;
typedef Vec<float, 4> Vec4f;
typedef Vec<float, 6> Vec6f;

typedef Vec<double, 2> Vec2d;
typedef Vec<double, 3> Vec3d;
typedef Vec<double, 4> Vec4d;
typedef Vec<double, 6> Vec6d;
```
例如 8U 类型的 RGB 彩色图像可以使用 Vec3b,3 通道 float 类型的矩阵可以使用 Vec3f。 对于 Vec 对象,可以使用[]符号如操作数组般读写其元素,如:
```
Vec3b color; //用 color 变量描述一种 RGB 颜色
color[0]=255; //B 分量
color[1]=0; //G 分量
color[2]=0; //R 分量
```

### 3.5像素值的读写
很多时候,我们需要读取某个像素值,或者设置某个像素值;在更多的时候,我们需要对整个图像里的所有像素进行遍历。OpenCV 提供了多种方法来实现图像的遍历。

#### 3.5.1 at()函数
函数 at()来实现读去矩阵中的某个像素,或者对某个像素进行赋值操作。下面两行代码演示了 at()函数的使用方法。
```
uchar value = grayim.at<uchar>(i,j);//读出第 i 行第 j 列像素值
grayim.at<uchar>(i,j)=128; //将第 i 行第 j 列像素值设置为 128
```
如果要对图像进行遍历,可以参考下面的例程。这个例程创建了两个图像,分别是单通道的 grayim 以及 3 个通道的colorim,然后对两个图像的所有像素值进行赋值,最后现实结果。
```
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    Mat grayim(600, 800, CV_8UC1);
    Mat colorim(600, 800, CV_8UC3);
    //遍历所有像素,并设置像素值
    for( int i = 0; i < grayim.rows; ++i)
        for( int j = 0; j < grayim.cols; ++j )
            grayim.at<uchar>(i,j) = (i+j)%255;
    //遍历所有像素,并设置像素值
    for( int i = 0; i < colorim.rows; ++i)
        for( int j = 0; j < colorim.cols; ++j )
        {
            Vec3b pixel;
            pixel[0] = i%255; //Blue
            pixel[1] = j%255; //Green
            pixel[2] = 0;
            //Red
            colorim.at<Vec3b>(i,j) = pixel;
        }
    //显示结果
    imshow("grayim", grayim);
    imshow("colorim", colorim);
    waitKey(0);
    return 0;
}
```

需要注意的是,如果要遍历图像,并不推荐使用 at()函数。使用这个函数的优点是代码的可读性高,但是效率并不是很高。 这段代码的运行结果如图 3.5 所示。

<img src='./images/Fig6.png' width='500' height='200'>

**图 3.5 使用 at()函数遍历图像的例程的输出结果**

#### 3.5.2 使用迭代器
如果你熟悉 C++的 STL 库,那一定了解迭代器(iterator)的使用。迭代器可以方便地遍历所有元素。Mat 也增加了迭代器的支持,以便于矩阵元素的遍历。下面的例程功能跟上一节的例程类似,但是由于使用了迭代器,而不是使用行数和列数来遍历,所以这儿没有了 i 和 j 变量,图像的像素值设置为一个随机数。
```
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    Mat grayim(600, 800, CV_8UC1);
    Mat colorim(600, 800, CV_8UC3);
    //遍历所有像素,并设置像素值
    MatIterator_<uchar> grayit, grayend;
    for(grayit=grayim.begin<uchar>(),grayend =grayim.end<uchar>(); grayit != grayend; ++grayit)
        *grayit = rand()%255;
    //遍历所有像素,并设置像素值
    MatIterator_<Vec3b> colorit, colorend;
    for(colorit=colorim.begin<Vec3b>(),colorend=colorim.end<Vec3b>(); colorit != colorend; ++colorit)
    {
        (*colorit)[0] = rand()%255; //Blue
        (*colorit)[1] = rand()%255; //Green
        (*colorit)[2] = rand()%255; //Red
    }
    //显示结果
    imshow("grayim", grayim);
    imshow("colorim", colorim);
    waitKey(0);
    return 0;
}

```

<img src='./images/Fig7.png' width='500' height='200'>

**图 3.6 使用迭代器遍历图像的例程的输出结果**

#### 3.5.3 通过数据指针
使用 IplImage 结构的时候,我们会经常使用数据指针来直接操作像素。通过指针操作来访问像素是非常高效的,但是你务必十分地小心。C/C++中的指针操作是不进行类型以及越界检查的,如果指针访问出错,程序运行时有时候可能看上去一切正常,有时候却突然弹出“段错误”(segment fault)。当程序规模较大,且逻辑复杂时,查找指针错误十分困难。对于不熟悉指针的编程者来说,指针就如同噩梦。如果你对指针使用没有自信,则不建议直接通过指针操作来访问像素。虽然 at()函数和迭代器也不能保证对像素访问进行充分的检查,但是总是比指针操作要可靠一些。如果你非常注重程序的运行速度,那么遍历像素时,建议使用指针。下面的例程演示如何使用指针来遍历图像中的所有像素。此例程实现的操作跟第 3.5.1节中的例程完全相同。例程代码如下:
```
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    Mat grayim(600, 800, CV_8UC1);
    Mat colorim(600, 800, CV_8UC3);
    //遍历所有像素,并设置像素值
    for( int i = 0; i < grayim.rows; ++i)
    {
        //获取第 i 行首像素指针
        uchar * p = grayim.ptr<uchar>(i);
        //对第 i 行的每个像素(byte)操作
        for( int j = 0; j < grayim.cols; ++j )
            p[j] = (i+j)%255;
    }
    //遍历所有像素,并设置像素值
    for( int i = 0; i < colorim.rows; ++i)
    {
        //获取第 i 行首像素指针
        Vec3b * p = colorim.ptr<Vec3b>(i);
        for( int j = 0; j < colorim.cols; ++j )
        {
            p[j][0] = i%255; //Blue
            p[j][1] = j%255; //Green
            p[j][2] = 0;
            //Red
        }
    }
    //显示结果
    imshow("grayim", grayim);
    imshow("colorim", colorim);
    waitKey(0);
    return 0;
}

```

<img src='./images/Fig8.png' width='500' height='200'>

**图 3.7 使用指针遍历图像的例程的输出结果**

### 3.6选取图像局部区域
Mat 类提供了多种方便的方法来选择图像的局部区域。使用这些方法时需要注意,这些方法并不进行内存的复制操作。如果将局部区域赋值给新的 Mat 对象,新对象与原始对象共用相同的数据区域,不新申请内存,因此这些方法的执行速度都比较快。

#### 3.6.1 单行或单列选择
提取矩阵的一行或者一列可以使用函数 row()或 col()。函数的声明如下:
```
Mat Mat::row(int i) const
Mat Mat::col(int j) const
```
参数 i 和 j 分别是行标和列标。例如取出 A 矩阵的第 i 行可以使用如下代码:
```
Mat line = A.row(i);
```
例如取出 A 矩阵的第 i 行,将这一行的所有元素都乘以 2,然后赋值给第 j行,可以这样写:
```
A.row(j) = A.row(i)*2;
```
####3.6.2 用 Range 选择多行或多列
Range 是 OpenCV 中新增的类,该类有两个关键变量 star 和 end。Range 对象可以用来表示矩阵的多个连续的行或
者多个连续的列。其表示的范围为从 start到 end,包含 start,但不包含 end。Range 类的定义如下:

```
    class Range
    {
    public:
        ...
        int start, end;
    };
```
Range 类还提供了一个静态方法 all(),这个方法的作用如同 Matlab 中的“:”,表示所有的行或者所有的列。
```
    //创建一个单位阵
    Mat A = Mat::eye(10, 10, CV_32S);
    //提取第 1 到 3 列(不包括 3)
    Mat B = A(Range::all(), Range(1, 3));
    //提取 B 的第 5 至 9 行(不包括 9)
    //其实等价于 C = A(Range(5, 9), Range(1, 3))
    Mat C = B(Range(5, 9), Range::all());
    Mat Rotation = post（Range（0,3）， Range(0,3))
```

#### 3.6.3 感兴趣区域
从图像中提取感兴趣区域(Region of interest)有两种方法,一种是使用构造函数,如下例所示:
```
//创建宽度为 320,高度为 240 的 3 通道图像
Mat img(Size(320,240),CV_8UC3);
//roi 是表示 img 中 Rect(10,10,100,100)区域的对象
Mat roi(img, Rect(10,10,100,100));
```
除了使用构造函数,还可以使用括号运算符,如下:
```
Mat roi2 = img(Rect(10,10,100,100));
```
当然也可以使用 Range 对象来定义感兴趣区域,如下:
```
//使用括号运算符
Mat roi3 = img(Range(10,100),Range(10,100));
//使用构造函数
Mat roi4(img, Range(10,100),Range(10,100));
```

#### 3.6.4 取对角线元素
矩阵的对角线元素可以使用 Mat 类的 diag()函数获取,该函数的定义如下:
```
Mat Mat::diag(int d) const
```
参数 d=0 时,表示取主对角线;当参数 d>0 是,表示取主对角线下方的次对角线,如 d=1 时,表示取主对角线下方,且紧贴主多角线的元素;当参数 d<0 时,表示取主对角线上方的次对角线。

如同 row()和 col()函数, diag()函数也不进行内存复制操作,其复杂度也是 O(1)。

### 3.7Mat 表达式
利用 C++中的运算符重载,OpenCV 2 中引入了 Mat 运算表达式。这一新特点使得使用 C++进行编程时,就如同写Matlab 脚本,代码变得简洁易懂,也便于维护。

如果矩阵 A 和 B 大小相同,则可以使用如下表达式:
```
C = A + B + 1;
```
其执行结果是 A 和 B 的对应元素相加,然后再加 1,并将生成的矩阵赋给 C变量。

下面给出 Mat 表达式所支持的运算。下面的列表中使用 A 和 B 表示 Mat 类型的对象,使用 s 表示 Scalar 对象,alpha表示 double 值。
- 加法,减法,取负:A+B,A-B,A+s,A-s,s+A,s-A,-A
- 缩放取值范围:A*alpha
- 矩阵对应元素的乘法和除法: A.mul(B),A/B,alpha/A
- 矩阵乘法:A*B (注意此处是矩阵乘法,而不是矩阵对应元素相乘)
- 矩阵转置:A.t()
- 矩阵求逆和求伪逆:A.inv()
- 矩阵比较运算:A cmpop B,A cmpop alpha,alpha cmpop A。此处 cmpop可以是>,>=,==,!=,<=,<。如果条件成立,则结果矩阵(8U 类型矩阵)的对应元素被置为 255;否则置 0。
- 矩阵位逻辑运算:A logicop B,A logicop s,s logicop A,~A,此处 logicop可以是&,|和^。
- 矩阵对应元素的最大值和最小值:min(A, B),min(A, alpha),max(A, B),max(A, alpha)。
- 矩阵中元素的绝对值:abs(A)
- 叉积和点积:A.cross(B),A.dot(B)

下面例程展示了 Mat 表达式的使用方法,例程的输出结果如图 3.8 所示。
```
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    Mat A = Mat::eye(4,4,CV_32SC1);
    Mat B = A * 3 + 1;
    Mat C = B.diag(0) + B.col(1);
    cout << "A = " << A << endl << endl;
    cout << "B = " << B << endl << endl;
    cout << "C = " << C << endl << endl;
    cout << "C .* diag(B) = " << C.dot(B.diag(0)) << endl;
    return 0;
}

```

<img src='./images/Fig9.png' width='250' height='300'>

**图 3.8 Mat 表达式例程的输出结果** 

### 3.8 Mat_ 类
Mat_类是对 Mat 类的一个包装,其定义如下:
```
template<typename _Tp> class Mat_ : public Mat
{
public:
    //只定义了几个方法
    //没有定义新的属性
};
```
这是一个非常轻量级的包装,既然已经有 Mat 类,为何还要定义一个 Mat_?下面我们看这段代码:
```
Mat M(600, 800, CV_8UC1);
for( int i = 0; i < M.rows; ++i)
{
    uchar * p = M.ptr<uchar>(i);
    for( int j = 0; j < M.cols; ++j )
    {
        double d1 = (double) ((i+j)%255);
        M.at<uchar>(i,j) = d1;
        double d2 = M.at<double>(i,j);//此行有错
    }
}
```
在读取矩阵元素时,以及获取矩阵某行的地址时,需要指定数据类型。这样首先需要不停地写“”,让人感觉很繁琐,在繁琐和烦躁中容易犯错,如上面代码中的错误,用 at()获取矩阵元素时错误的使用了 double 类型。这种错误不是语法错误,因此在编译时编译器不会提醒。在程序运行时,at()函数获取到的不是期望的(i,j)位置处的元素,数据已经越界,但是运行时也未必会报错。这样的错误使得你的程序忽而看上去正常,忽而弹出“段错误”,特别是在代码规模很大时,难以查错。

如果使用 Mat_类,那么就可以在变量声明时确定元素的类型,访问元素时不再需要指定元素类型,即使得代码简洁,又减少了出错的可能性。上面代码可以用 Mat_实现,实现代码如下面例程里的第二个双重 for 循环。
```
#include <iostream>
#include "opencv2/opencv.hpp"
#include <stdio.h>
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    Mat M(600, 800, CV_8UC1);
    for( int i = 0; i < M.rows; ++i)
    {
        //获取指针时需要指定类型
        uchar * p = M.ptr<uchar>(i);
        for( int j = 0; j < M.cols; ++j )
        {
            double d1 = (double) ((i+j)%255);
            //用 at()读写像素时,需要指定类型
            M.at<uchar>(i,j) = d1;
            //下面代码错误,应该使用 at<uchar>()
            //但编译时不会提醒错误
            //运行结果不正确,d2 不等于 d1
            double d2 = M.at<double>(i,j);
        }
    }
    //在变量声明时指定矩阵元素类型
    Mat_<uchar> M1 = (Mat_<uchar>&)M;
    for( int i = 0; i < M1.rows; ++i)
    {
        //不需指定元素类型,语句简洁
        uchar * p = M1.ptr(i);
        for( int j = 0; j < M1.cols; ++j )
        {
            double d1 = (double) ((i+j)%255);
            //直接使用 Matlab 风格的矩阵元素读写,简洁
            M1(i,j) = d1;
            double d2 = M1(i,j);
        }
    }
    return 0;
}
```

### 3.9Mat 类的内存管理
使用 Mat 类,内存管理变得简单,不再像使用 IplImage 那样需要自己申请和释放内存。虽然不了解 Mat 的内存管理机制,也无碍于 Mat 类的使用,但是如果清楚了解 Mat 的内存管理,会更清楚一些函数到底操作了哪些数据。

Mat 是一个类,由两个数据部分组成:矩阵头(包含矩阵尺寸,存储方法,存储地址等信息)和一个指向存储所有像素值的矩阵的指针,如图 3.9 所示。矩阵头的尺寸是常数值,但矩阵本身的尺寸会依图像的不同而不同,通常比矩阵头的尺寸大数个数量级。复制矩阵数据往往花费较多时间,因此除非有必要,不要复制大的矩阵。

为了解决矩阵数据的传递,OpenCV 使用了引用计数机制。其思路是让每个Mat 对象有自己的矩阵头信息,但多个 Mat对象可以共享同一个矩阵数据。让矩阵指针指向同一地址而实现这一目的。很多函数以及很多操作(如函数参数传值)只复制矩阵头信息,而不复制矩阵数据。

前面提到过,有很多中方法创建 Mat 类。如果 Mat 类自己申请数据空间,那么该类会多申请 4 个字节,多出的 4 个字节存储数据被引用的次数。引用次数存储于数据空间的后面,refcount 指向这个位置,如图 3.9 所示。当计数等于 0 时,则释放该空间。

<img src='./images/Fig10.png' width='520' height='220'>

**图 3.9 Mat 类中的数据存储示意图 ,refcount 变量指向数据区后面 ,用 4 个字节 (int 类型 )存储引用数目。**

关于多个矩阵对象共享同一矩阵数据,我们可以看这个例子:
```
Mat A(100,100, CV_8UC1);
Mat B = A;
Mat C = A(Rect(50,50,30,30));
```
上面代码中有三个 Mat 对象,分别是 A, B 和 C。这三者共有同一矩阵数据,其示意图如图 3.10 所示。

<img src='./images/Fig11.png' width='350' height='270'>

**图 3.10 三个矩阵头共用共用同一矩阵数据**

### 3.10 输出
从前面的例程中,可以看到 Mat 类重载了<<操作符,可以方便得使用流操作来输出矩阵的内容。默认情况下输出的格式是类似 Matlab 中矩阵的输出格式。除了默认格式,Mat 也支持其他的输出格式。代码如下:

首先创建一个矩阵,并用随机数填充。填充的范围由 randu()函数的第二个参数和第三个参数确定,下面代码是介于 0到 255 之间。
```
Mat R = Mat(3, 2, CV_8UC3);
randu(R, Scalar::all(0), Scalar::all(255));
```
默认格式输出的代码如下:
```
cout << "R (default) = " << endl << R << endl << endl;
```
输出结果如图 3.11 所示。

<img src='./images/Fig12.png' width='246' height='67'>

**图 3.11 默认格式的矩阵输出**
Python 格式输出的代码如下: 

```
cout << "R (python)= " << endl << format(R,"python") << endl << endl;
```

<img src='./images/Fig13.png' width='291' height='68'>

**图 3.12 Python 格式的矩阵输出**

除了 Mat 对象可以使用<<符号输出,其他的很多类型也支持<<输出。 

二维点:
```
Point2f P(5, 1);
cout << "Point (2D) = " << P << endl << endl;
```

<img src='./images/Fig14.png' width='159' height='18'>

**图 3.17 二维点的输出结果**
三维点:
```
Point3f P3f(2, 6, 7);
cout << "Point (3D) = " << P3f << endl << endl;
```

<img src='./images/Fig15.png' width='182' height='18'>

**图 3.17 三维点的输出结果**

## 4 数据获取与存储

### 4.1读写图像文件
将图像文件读入内存,可以使用 imread()函数;将 Mat 对象以图像文件格式写入内存,可以使用 imwrite()函数。

#### 4.1.1 读图像文件
imread()函数返回的是 Mat 对象,如果读取文件失败,则会返回一个空矩阵,即 Mat::data 的值是 NULL。执行 imread()之后,需要检查文件是否成功读入,你可以使用 Mat::empty()函数进行检查。imread()函数的声明如下:
```
Mat imread(const string& filename, int flags=1 )
```
很明显参数 filename 是被读取或者保存的图像文件名;在 imread()函数中,flag 参数值有三种情况:
- flag>0,该函数返回 3 通道图像,如果磁盘上的图像文件是单通道的灰度图像,则会被强制转为 3 通道;
- flag=0,该函数返回单通道图像,如果磁盘的图像文件是多通道图像,则会被强制转为单通道;
- flag<0,则函数不对图像进行通道转换。 imread()函数支持多种文件格式,且该函数是根据图像文件的内容来确定文件格式,而不是根据文件的扩展名来确定。所只是的文件格式如下: -Windows 位图文件 - BMP, DIB;
- JPEG 文件 - JPEG, JPG, JPE;
- 便携式网络图片 - PNG;
- 便携式图像格式 - PBM,PGM,PPM;
- Sun rasters - SR,RAS;
- TIFF 文件 - TIFF,TIF;
- OpenEXR HDR 图片 - EXR;
- JPEG 2000 图片- jp2。 你所安装的 OpenCV 并不一定能支持上述所有格式,文件格式的支持需要特定的库,只有在编译 OpenCV 添加了相应的文件格式库,才可支持其格式。

#### 4.1.2 写图像文件
将图像写入文件,可使用 imwrite()函数,该函数的声明如下:
```
bool imwrite(const string& filename, InputArray image,
const vector<int>& params=vector<int>())
```
文件的格式由 filename 参数指定的文件扩展名确定。推荐使用 PNG 文件格式。BMP 格式是无损格式,但是一般不进行压缩,文件尺寸非常大;JPEG 格式的文件娇小,但是 JPEG 是有损压缩,会丢失一些信息。PNG 是无损压缩格式,推荐使用。
imwrite()函数的第三个参数 params 可以指定文件格式的一些细节信息。这个参数里面的数值是跟文件格式相关的:

- JPEG:表示图像的质量,取值范围从 0 到 100。数值越大表示图像质量越高,当然文件也越大。默认值是 95。
- PNG:表示压缩级别,取值范围是从 0 到 9。数值越大表示文件越小,但是压缩花费的时间也越长。默认值是3。
- PPM,PGM 或 PBM:表示文件是以二进制还是纯文本方式存储,取值为0 或 1。如果取值为 1,则表示以二进制方式存储。默认值是 1。

并不是所有的 Mat 对象都可以存为图像文件,目前支持的格式只有 8U 类型的单通道和 3 通道(颜色顺序为 BGR)矩阵;如果需要要保存 16U 格式图像,只能使用 PNG、JPEG 2000 和 TIFF 格式。如果希望将其他格式的矩阵保存为图像文件,可以先用 Mat::convertTo()函数或者 cvtColor()函数将矩阵转为可以保存的格式。

另外需要注意的是,在保存文件时,如果文件已经存在,imwrite()函数不会进行提醒,将直接覆盖掉以前的文件。下面例程展示了如何读入一副图像,然后对图像进行 Canny 边缘操作,最后将结果保存到图像文件中。

```
#include <iostream>
#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;
int main(int argc, char* argv[])
{
    //读入图像,并将之转为单通道图像
    Mat im = imread("lena.jpg", 0);
    //请一定检查是否成功读图
    if( im.empty() )
    {
        cout << "Can not load image." << endl;
        return -1;
    }
    //进行 Canny 操作,并将结果存于 result
    Mat result;
    Canny(im, result, 50, 150);
    //保存结果
    imwrite("lena-canny.png", result);
    return 0;
}
```

<img src='./images/Fig16.png' width='300' height='300'>

**图 4.1 Lena 图像的边缘提取结果**

## 5 进阶
很多初学者希望快速掌握 OpenCV 的使用方法,但往往会遇到各种各样的困难。其实仔细分析,造成这些困难的原因有两类:第一类是 C/C++编程基础不过关;第二类是不了解算法原理。解决这些困难无非提升编程能力,以及提升理论基础知识。提升编程能力需要多练习编程,提升理论知识需要系统学习《数字图像处理》、《计算机视觉》和《模式识别》等课程,所有这些都不能一蹴而就,需要耐下心来认真修炼。 本文为OpenCV入门教程更多内容请前往

* [OpenCV Tutorials](https://docs.opencv.org/2.4/doc/tutorials/tutorials.html)
* [OpenCV Tutorials 中文](http://www.opencv.org.cn/opencvdoc/2.3.2/html/doc/tutorials/tutorials.html)

## 6 OpenCV画图函数
### 6.1直线

```
void line(Mat& img, Point pt1, Point pt2, const Scalar& color, int thickness=1, int lineType=8, int shift=0)
```

img: 要绘制线段的图像。

pt1: 线段的起点。

pt2: 线段的终点。
color: 线段的颜色，通过一个Scalar对象定义。
thickness: 线条的宽度。
lineType: 线段的类型。可以取值8，4， 和CV_AA， 分别代表8邻接连接线，4邻接连接线和反锯齿连接线。默认值为8邻接。为了获得更好地效果可以选用CV_AA(采用了高斯滤波)。
shift: 坐标点小数点位数。

### 6.2矩形

```
void rectangle(Mat& img, Point pt1,Point pt2,const Scalar& color, int thickness=1, int lineType=8, int shift=0);

void rectangle(Mat& img, Rect rec, const Scalar& color, int thickness=1, int lineType=8, int shift=0 );

```

img 图像。
pt1 矩形的一个顶点。
pt2 矩形对角线上的另一个顶点。
rec 矩形区域（x,y,width,height)。
color 线条颜色 (RGB) 或亮度（灰度图像 ）(grayscale image。
thickness 组成矩形的线条的粗细程度。取负值时（如 CV_FILLED）函数绘制填充了色彩的矩形。
line_type 线条的类型；
shift 坐标点的小数点位数。

### 6.2圆：（标记特征点常用)

```
void circle(CV_IN_OUT Mat& img, Point center, int radius, const Scalar& color, int thickness=1, int lineType=8, int shift=0); 
```

img 图像。
 center 画圆的圆心坐标。
 radius 圆的半径。
 color为 圆的颜色。
 thickness 圆线条的粗细，值越大则线条越粗，为负数则是填充效果
 line_type 线条的类型；见cvLine的描述。
 shift 坐标点的小数点位数。

## 7 OpenCV在SLAM中的应用

### 7.1图片读取

cv::assert()计算括号内的表达式，如果表达式为FALSE (或0), 程序将报告错误，并终止执行。如果表达式不为0，则继续执行后面的语句；

常用来判断读取图片是否成功：

```
cv::assert(m.data());
```

### 7.2keypoint类

其默认构造函数为：CV_WRAP KeyPoint() : pt(0,0), size(0),  angle(-1), response(0), octave(0), class_id(-1) {}  

其中pt(x,y):关键点的点坐标；  

size():该关键点邻域直径大小；

angle:角度，表示关键点的方向，值为[0,360)，负值表示不使用。

 response:响应强度，选择响应最强的关键点;   

octacv:从哪一层金字塔得到的此关键点。

class_id:当要对图片进行分类时，用class_id对每个关键点进行区分，默认为-1。

### 7.3　　特征点提取及匹配

Ptr<FeatureDetector> detector = ”等价于 “FeatureDetector * detector =”。Ptr是OpenCV中使用的智能指针模板类，可以轻松管理各种类型的指针。

特征检测器FeatureDetetor是虚类，通过定义FeatureDetector的对象可以使用多种特征检测及匹配方法，通过create()函数调用。

```
    vector<KeyPoint> kp1first, kp1;
    Ptr<FastFeatureDetector> detector = FastFeatureDetector::create(12, true,1);
    detector->detect(img1, kp1first);
```

描述子提取器DescriptorExtractor是提取关键点的描述向量类抽象基类。描述子匹配器DescriptorMatcher用于特征匹配，"BruteForce-Hamming"表示使用汉明距离进行匹配。computer()计算关键点的描述子向量

```
 Ptr<DescriptorExtractor> descriptor = ORB::create();
 Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
 descriptor->compute(img_1, keypoints_1, descriptors_1);
```

DMatch是匹配关键点描述子 类, matches用于存放匹配项。

```
vector<DMatch> matches;    //DMatch是匹配关键点描述子 类, matches用于存放匹配项
matcher->match(descriptors_1, descriptors_2, matches);
```

 drawMatches用于绘制两幅图像的匹配关键点。参数1是第一个源图像，参数2是其关键点数组；参数3是第二张原图像，参数4是其关键点数组。参数5是两张图像的匹配关键点数组,参数6用于存放函数的绘制结果。

```
 drawMatches(img_1, keypoints_1, img_2, keypoints_2, matches, img_match);
```

# 8 课后作业及参考代码

修改课上扣图标代码，使用其他方法判定感兴趣区域，使其更加通用

[参考代码](./opencvtest)