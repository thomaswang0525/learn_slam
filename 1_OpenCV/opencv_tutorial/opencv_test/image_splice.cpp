#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>

using namespace std;
using namespace cv;

int main()
{
    cv::Mat image1, image2;
    cv::Mat H;
    image1=imread("../picture/image1.png", IMREAD_ANYCOLOR);
    image2=imread("../picture/image2.png", IMREAD_ANYCOLOR);
    assert(!image1.empty() && !image2.empty());
    imshow("image1", image1);
    imshow("image2", image2);

    vector<KeyPoint> kp1, kp2;
    vector<DMatch> matchs;
    Mat descroptor1, descroptor2;
    Ptr<FeatureDetector> detector = ORB::create();
    Ptr<DescriptorExtractor> decriptor = ORB::create();
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    detector->detect(image1, kp1);
    detector->detect(image2, kp2);
    Mat image3, image4;
    drawKeypoints(image1,kp1,image3,Scalar::all(-1),DrawMatchesFlags::DEFAULT);
    drawKeypoints(image2,kp2,image4,Scalar::all(-1),DrawMatchesFlags::DEFAULT);
    imshow("image1 keypoints", image3);
    imshow("image2 keypoints", image4);

    decriptor->compute(image1,kp1,descroptor1);
    decriptor->compute(image2,kp2,descroptor2);

    matcher->match(descroptor1,descroptor2,matchs);

    auto min_max= minmax_element(matchs.begin(), matchs.end(), [](const DMatch &m1, const DMatch &m2) {return m1.distance <m2.distance;});
    double min_dist = min_max.first->distance;
    vector<DMatch> good_matches;
    for(size_t i=0; i<matchs.size(); i++){
        if(matchs[i].distance <= max(2*min_dist, 30.0)){
            good_matches.push_back(matchs[i]);
        }
    }
    vector<Point2f> goodkp1, goodkp2;
    for(size_t i=0; i<good_matches.size(); i++){
        goodkp1.push_back(kp1.at(good_matches[i].queryIdx).pt);
        goodkp2.push_back(kp2.at(good_matches[i].trainIdx).pt);
    }

    Mat H12=findHomography(goodkp2, goodkp1, 0, 3,noArray(),2000, 0.995);
    cout<<H12 <<endl;


    //图像配准
    Mat imageTransform1, imageTransform2;
    cv::Mat corner00=(Mat_<double>(3,1)<<0,0,1);
    cv::Mat corner01=(Mat_<double>(3,1)<<image2.cols,0,1);
    cv::Mat corner10=(Mat_<double>(3,1)<<0,image2.rows,1);
    cv::Mat corner11=(Mat_<double>(3,1)<<image2.cols,image2.rows,1);
    cv::Mat newcorner00 = H12 * corner00;
    cv::Mat newcorner01 = H12 * corner01;
    cv::Mat newcorner10 = H12 * corner10;
    cv::Mat newcorner11 = H12 * corner11;
    
    newcorner00=newcorner00/newcorner00.at<double>(2,0);
    newcorner01=newcorner01/newcorner01.at<double>(2,0);
    newcorner10=newcorner10/newcorner10.at<double>(2,0);
    newcorner11=newcorner11/newcorner11.at<double>(2,0);

    int maxx = ceil(max(newcorner01.at<double>(0,0), newcorner11.at<double>(0,0)));
    int maxy = ceil(max(newcorner01.at<double>(1,0), newcorner11.at<double>(1,0)));
    cv::Mat dst;
    warpPerspective(image2,dst,H12,Size(maxx, maxy));
    cv::Mat pimage2=dst.clone();
    image1.copyTo(dst(Rect(0, 0, image1.cols, image1.rows)));

    imshow("warpPerspective ", dst);

    int start = min(newcorner00.at<double>(0,0), newcorner10.at<double>(0,0));
    double processWidth = image1.cols -start;  //重叠区域的列宽度
    double alpha=1;

    for(int i=0; i<image1.rows; i++){
        uchar* p=image1.ptr<uchar>(i);
        uchar* p2=pimage2.ptr<uchar>(i);
        uchar* r=dst.ptr<uchar>(i);

        for(int j=start; j<image1.cols; j++){
            if(p2[j*3]==0 && p2[j*3+1]==0 && p2[j*3+2]==0){
                alpha=1;
            }else{
                alpha=(processWidth-(j-start))/processWidth;
            }
              r[j * 3] = p[j * 3] * (alpha) + p2[j * 3] * (1 - alpha);
              r[j * 3 + 1] = p[j * 3 + 1] * alpha + p2[j * 3 + 1] * (1 - alpha);
              r[j * 3 + 2] = p[j * 3 + 2] * alpha + p2[j * 3 + 2] * (1 - alpha);
        }
    }

    imshow("optimizer ", dst);


    cv::waitKey();
    cout << "Hello World!" << endl;
    return 0;
}