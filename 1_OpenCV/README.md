#  OpenCV学习

OpenCV 的全称是 Open Source Computer Vision Library,是一个开放源代码的 计算机视觉库。OpenCV 是最初由英特尔公司发起并开发,以 BSD 许可证授权发行,可以在商业和研究领域中免费使用,现在美国 Willow Garage 为OpenCV 提供主要的支持。OpenCV 可用于开发实时的图像处理、计算机视觉以及模式识别程序,目前在工业界以及科研领域广泛采用。 简单了说,OpenCV 其实就是一堆 C 和 C++语言的源代码文件,这些源代码文件中实现了许多常用的计算机视觉算法。例如 C 接口函数 cvCanny()实现了 Canny 边缘提取算法。可以直接将这些源代码添加到我们自己的软件项目中,而不需要自己再去写代码实现 Canny 算法,也就是不需要重复“造轮子”。

在学习OpenCV之前，需要把C++基础打扎实，学习的材料可以参考：
* [C++基础](https://gitee.com/pi-lab/learn_programming)
* [C++11](https://gitee.com/pi-lab/SummerCamp/tree/master/cpp/cpp11)


## 目标

1. 通过基础入门的学习，进一步加强对c++的理解
2. 学会使用OpenCV的基本操作与一些简单的原理



## 学习内容
* [OpenCV的基本，`Mat`以及对应的基本操作](https://gitee.com/pi-lab/SummerCamp/tree/master/slam/cv#3--%E5%9B%BE%E5%83%8F%E7%9A%84%E5%9F%BA%E6%9C%AC%E6%93%8D%E4%BD%9C)
* [读取图像，显示图像](https://gitee.com/pi-lab/SummerCamp/tree/master/slam/cv#4-%E6%95%B0%E6%8D%AE%E8%8E%B7%E5%8F%96%E4%B8%8E%E5%AD%98%E5%82%A8)
* [特征点提取，特征点匹配](https://blog.csdn.net/qq_38023849/article/details/107309790)
* [相机标定原理与程序](https://gitee.com/pi-lab/SummerCamp/tree/master/slam/camera)
* Fundamental Matrix， Essential Matrix
* 简单的图像拼接（利用Homograph等）