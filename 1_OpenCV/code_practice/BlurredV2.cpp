#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char **argv) {

	Mat dst, src;
	src = imread("F:/opencv/car.jpg");
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";
	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	//medianBlur(src,dst,3);
	bilateralFilter(src,dst,15,150,3);
	namedWindow(outputTitle, CV_WINDOW_AUTOSIZE);
	imshow(outputTitle, dst);

	Mat ResultImg;
	Mat kernel = (Mat_<int>(3, 3) << 0, -1, 0, -1, 5, -1, 0, -1, 0);
	filter2D(dst, ResultImg,-1,kernel,Point(-1,-1),0);
	imshow(" Final", ResultImg);

	waitKey(0);
	return 0;
}
