/****************************
 * 题目：相机视场角比较小（比如手机摄像头）时，一般可以近似为针孔相机成像，三维世界中的直线成像也是直线。
 * 但是很多时候需要用到广角甚至鱼眼相机，此时会产生畸变，三维世界中的直线在图像里会弯曲。因此，需要做去畸变。
 * 给定一张广角畸变图像，以及相机的内参，请完成图像去畸变过程
 *
* 本程序学习目标：
 * 掌握图像去畸变原理
****************************/
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std;
string image_file = "../test.png";   // 请确保路径正确

class Vins{
public:
    Vins(){}
    ~Vins(){}

    void VinsUndistorted(cv::Point2d& u_v);
private:
    double k1 = -0.28340811, k2 = 0.07395907;  // 畸变参数
    double fx = 458.654, fy = 457.296, cx = 367.215, cy = 248.375;
    double p1 = 0.0,p2=0.0;
    void VinsDistortion(const cv::Point2d& p_u,cv::Point2d& d_u);
};

void Vins::VinsDistortion(const cv::Point2d& p_u, cv::Point2d& d_u){

    double mx2_u, my2_u, mxy_u, rho2_u, rad_dist_u;

    mx2_u = p_u.x * p_u.x;
    my2_u = p_u.y * p_u.y;
    mxy_u = p_u.x * p_u.y;
    rho2_u = mx2_u + my2_u;
    rad_dist_u = k1 * rho2_u + k2 * rho2_u * rho2_u;
    d_u.x =  p_u.x * rad_dist_u + 2.0 * p1 * mxy_u + p2 * (rho2_u + 2.0 * mx2_u);
    d_u.y =  p_u.y * rad_dist_u + 2.0 * p2 * mxy_u + p1 * (rho2_u + 2.0 * my2_u);
}

void Vins::VinsUndistorted(cv::Point2d& u_v){
    
    double mx_d = (u_v.x - cx)/fx;
    double my_d = (u_v.y - cy)/fy;

    double mx_u ;
    double my_u ;

    int n = 8;
    cv::Point2d d_u;
    // 这里mx_d + du = 畸变后
    VinsDistortion(cv::Point2d(mx_d, my_d), d_u);
    // Approximate value
    mx_u = mx_d - d_u.x;
    my_u = my_d - d_u.y;

    for (int i = 1; i < n; ++i)
    {
        VinsDistortion(cv::Point2d(mx_u, my_u), d_u);
        mx_u = mx_d - d_u.x;
        my_u = my_d - d_u.y;
    }
    u_v.x = mx_u*fx+cx;
    u_v.y = my_u*fy+cy;
}

int main(int argc, char **argv) {
 
    double k1 = -0.28340811, k2 = 0.07395907;  // 畸变参数
    double fx = 458.654, fy = 457.296, cx = 367.215, cy = 248.375;

    cv::Mat image = cv::imread(image_file, CV_8UC1);   // 图像是灰度图
    int rows = image.rows, cols = image.cols;
    cv::Mat image_undistort = cv::Mat(rows, cols, CV_8UC1);   // 去畸变以后的图
    cv::Mat image_undistort_vins = cv::Mat(rows, cols, CV_8UC1);
    cv::imshow("image distorted", image);
    // 计算去畸变后图像的内容
    // 通过申请一个新的内存，存储的是未畸变的图片，通过未畸变的位置套入公式去计算畸变图片中的位置，然后填入新图片
    chrono::steady_clock::time_point t1 = chrono::steady_clock::now();
    for (int v = 0; v < rows; v++)
        for (int u = 0; u < cols; u++) {

            double u_distorted = 0, v_distorted = 0;
            // 开始代码，注意(u,v)要先转化为归一化坐标
            double x1 = (u -cx)/fx; 
            double y1 = (v -cy)/fy;
            double r = std::sqrt(x1 * x1 + y1 * y1);

            double x2 = x1 * (1+k1*pow(r,2)+k2*pow(r,4));
            double y2 = y1 * (1+k1*pow(r,2)+k2*pow(r,4));

            u_distorted = x2 * fx + cx;
            v_distorted = y2 * fy + cy;
            // 结束代码

            if (u_distorted >= 0 && v_distorted >= 0 && u_distorted < 2*cols && v_distorted < 2*rows) {
                image_undistort.at<uchar>(v, u) = image.at<uchar>((int) v_distorted, (int) u_distorted);
            } else {
                image_undistort.at<uchar>(v, u) = 0;
            }
        }
    chrono::steady_clock::time_point t2 = chrono::steady_clock::now();
    chrono::duration<double> time_used = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
    cout << "undistorted time cost = " << time_used.count() << " seconds. " << endl;
    cv::imshow("image undistorted", image_undistort);

    //Vins-mono去畸变的方式
    //已知畸变后的点通过迭代的方式去反推正确的位置
    Vins vins_;
    t1 = chrono::steady_clock::now();
    for (int v = 0; v < rows; v++)
        for (int u = 0; u < cols; u++) {
            cv::Point2d u_v;
            u_v.x = (double)u;
            u_v.y = (double)v;
            vins_.VinsUndistorted(u_v);
            if (u_v.x >= 0 && u_v.y >= 0 && u_v.x < cols && u_v.y < rows) {
                image_undistort_vins.at<uchar>((int)u_v.y,(int)u_v.x) = image.at<uchar>(v, u);
            }
        }
    t2 = chrono::steady_clock::now();
    time_used = chrono::duration_cast<chrono::duration<double>>(t2 - t1);
    cout << "undistorted time cost = " << time_used.count() << " seconds. " << endl;

    cv::imshow("image image_undistort_vins", image_undistort_vins);
    cv::waitKey();

    return 0;
}

