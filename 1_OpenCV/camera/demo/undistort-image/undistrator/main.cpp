#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char **argv) {
 
    const cv::Mat K = ( cv::Mat_<double> ( 3,3 ) << 458.654, 0.0, 367.215, 0.0, 457.296, 248.375, 0.0, 0.0, 1.0 );
    const cv::Mat D = ( cv::Mat_<double> ( 5,1 ) <<  -0.28340811, 0.07395907, 0.0, 0.00019359, 1.76187114e-05 );    

    cv::Mat rawimg=imread("../distort.png",IMREAD_COLOR);
    if(!rawimg.data)
    {
        cout<<"can't input the image"<<endl;
        return -1;
    }
    imshow("rawimage",rawimg);
     
    
    Size imageSize(rawimg.cols, rawimg.rows);
    const double alpha=0;
    Mat undistortimg=Mat(rawimg.size(),rawimg.type());
    //undistort(rawimg,undistortimg,K,D,K);
    //如果undistort函数的最后一个参数使用原相机内参，那么得到alpha=0的情况。
    //如果undistort函数的最后一个参数使用getOptimalNewCameraMatrix计算出来的新矩阵，那么得到损失像素后的图像，当alpha=1时的结果。
    //undistort函数内部调用了initUndistortRectifyMap和remap，处理多张图片时会降低效率，增加程序耗时。
        
    Mat map1,map2;
    Mat NewCameraMatrix = getOptimalNewCameraMatrix(K, D, imageSize, alpha, imageSize, 0);
    
    initUndistortRectifyMap(K, D, cv::Mat(), NewCameraMatrix, imageSize, CV_16SC2, map1, map2);
    
    remap(rawimg,undistortimg,map1,map2,cv::INTER_LINEAR);
    
    imshow("undistort image",undistortimg);
    imwrite("../undistort image.png", undistortimg);
    waitKey(0);
    
    return 0;
}
